package game.controller;

import game.view.KeyboardCallback;

/**
 * Created by nicolalasagni on 04/03/2017.
 */
public interface GameController extends KeyboardCallback {

    void updateGame();

}
