package game.controller;

import game.component.ImageComponent;
import game.component.character.EnemyCharacter;
import game.component.character.MainCharacter;
import game.model.ComponentConfiguration;
import game.model.Position;
import game.model.Screen;
import game.view.GameView;
import game.view.KeyboardListener;
import game.view.model.GameImageViewModel;
import game.view.model.GameViewModel;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nicolalasagni on 04/03/2017.
 */
public class GameControllerImpl extends SimpleComponentsController implements GameController {

    private ImageComponent castle;
    private ImageComponent flag;
    private ImageComponent start;
    private GameView gameView;

    public GameControllerImpl(GameView gameView, Screen screen, ComponentConfiguration componentConfiguration) {
        super(screen, componentConfiguration);
        this.castle = componentConfiguration.getCastle();
        this.flag = componentConfiguration.getFlag();
        this.start = componentConfiguration.getStart();
        this.gameView = gameView;
        KeyboardListener keyboardListener = new KeyboardListener(this);
        this.gameView.setKeyListener(keyboardListener);
    }

    @Override
    public void updateGame() {
        updateComponents();
        updateView();
    }

    public void updateComponents() {
        computeComponentsCollisions();
        moveBackground();
        moveComponents();
    }

    private void updateView() {
        GameViewModel viewModel = createGameViewModel();
        this.gameView.renderViewModel(viewModel);
    }

    private GameViewModel createGameViewModel() {
        GameViewModel viewModel = new GameViewModel();
        Screen screen = getScreen();
        int mainXPosition = screen.getMainXPosition();
        List<GameImageViewModel> imageViewModels = new ArrayList<>();
        imageViewModels.add(createImageViewModel(firstBackground.getImage(), firstBackground.getX(), firstBackground.getY()));
        imageViewModels.add(createImageViewModel(secondBackground.getImage(), secondBackground.getX(), firstBackground.getY()));
        imageViewModels.add(createImageViewModel(start.getImage(), start.getX() - mainXPosition, start.getY()));
        imageViewModels.addAll(createImageViewModels(this.staticComponents));
        imageViewModels.addAll(createImageViewModels(this.coins));
        imageViewModels.add(createImageViewModel(flag.getImage(), flag.getX() - mainXPosition, flag.getY()));
        imageViewModels.add(createImageViewModel(castle.getImage(), castle.getX() - mainXPosition, castle.getY()));
        imageViewModels.add(createMainCharacterViewModel(this.mario));
        imageViewModels.add(createEnemyCharacterViewModel(this.mushroom));
        imageViewModels.add(createEnemyCharacterViewModel(this.turtle));
        viewModel.setImageViewModels(imageViewModels);
        return viewModel;
    }

    private List<GameImageViewModel> createImageViewModels(List<ImageComponent> imageComponents) {
        List<GameImageViewModel> imageViewModels = new ArrayList<>();
        for(ImageComponent imageComponent : imageComponents) {
            imageViewModels.add(createImageViewModel(imageComponent.getImage(), imageComponent.getX(), imageComponent.getY()));
        }
        return imageViewModels;
    }

    private GameImageViewModel createMainCharacterViewModel(MainCharacter mainCharacter) {
        Image image = mainCharacter.getAliveImage();
        int x = mainCharacter.getX();
        int y = mainCharacter.getY();
        if (mainCharacter.isJumping()) {
            image = mainCharacter.jump();
        }
        return createImageViewModel(image, x, y);
    }

    private GameImageViewModel createEnemyCharacterViewModel(EnemyCharacter enemyCharacter) {
        Image image = enemyCharacter.getAliveImage();
        int x = enemyCharacter.getX();
        int y = enemyCharacter.getY();
        if (!enemyCharacter.getCharacterState().isAlive()) {
            image = enemyCharacter.getDeadImage();
            y = enemyCharacter.getDeadY();
        }
        return createImageViewModel(image, x, y);
    }

    private GameImageViewModel createImageViewModel(Image image, int x, int y) {
        Position position = new Position(x, y);
        return new GameImageViewModel(image, position);
    }

    @Override
    public void onButtonPressed(ButtonType buttonType) {
        if (this.mario.getCharacterState().isAlive()) {
            switch (buttonType) {
                case RIGHT:
                    moveMainCharacterRight();
                    break;
                case LEFT:
                    moveMainCharacterLeft();
                    break;
                case UP:
                    makeMainCharacterJump();
                    break;
            }
        }
    }

    @Override
    public void onButtonsReleased() {
        stopMainCharacter();
    }

}
