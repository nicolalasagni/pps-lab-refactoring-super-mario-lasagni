package game.controller;

/**
 * Created by nicolalasagni on 04/03/2017.
 */
public interface ComponentsController {

    void updateComponents();

    void computeComponentsCollisions();

    void moveBackground();

    void moveComponents();

    void moveMainCharacterLeft();

    void moveMainCharacterRight();

    void makeMainCharacterJump();

    void stopMainCharacter();

}
