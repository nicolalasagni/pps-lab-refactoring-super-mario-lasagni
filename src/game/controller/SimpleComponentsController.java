package game.controller;

import game.component.ImageComponent;
import game.component.character.EnemyCharacter;
import game.component.character.MainCharacter;
import game.component.object.Background;
import game.model.ComponentConfiguration;
import game.model.Screen;
import util.Audio;

import java.util.List;

/**
 * Created by nicolalasagni on 04/03/2017.
 */
public abstract class SimpleComponentsController implements ComponentsController {

    private Screen screen;
    private ComponentConfiguration componentConfiguration;
    ImageComponent firstBackground;
    ImageComponent secondBackground;
    MainCharacter mario;
    EnemyCharacter mushroom;
    EnemyCharacter turtle;
    List<ImageComponent> staticComponents;
    List<ImageComponent> coins;

    SimpleComponentsController(Screen screen, ComponentConfiguration componentConfiguration) {
        this.screen = screen;
        this.componentConfiguration = componentConfiguration;
        this.firstBackground = componentConfiguration.getFirstBackground();
        this.secondBackground = componentConfiguration.getSecondBackground();
        this.mario = componentConfiguration.getMainCharacter();
        this.mushroom = componentConfiguration.getMushroom();
        this.turtle = componentConfiguration.getTurtle();
        this.staticComponents = componentConfiguration.getStaticComponents();
        this.coins = componentConfiguration.getCoins();
    }

    Screen getScreen() {
        return this.screen;
    }

    private List<ImageComponent> getStaticComponents() {
        return this.componentConfiguration.getStaticComponents();
    }

    private List<ImageComponent> getCoins() {
        return this.componentConfiguration.getCoins();
    }

    @Override
    public abstract void updateComponents();

    @Override
    public void computeComponentsCollisions() {
        computeStaticComponentCollisions();
        computeCoinCollisions();
        computeCharacterCollisions();
    }

    private void computeStaticComponentCollisions() {
        List<ImageComponent> staticComponents = getStaticComponents();
        for (ImageComponent staticComponent : staticComponents) {
            if (this.mario.isNearTo(staticComponent)) {
                this.mario.collisionWith(staticComponent);
            }
            if (this.mushroom.isNearTo(staticComponent)) {
                this.mushroom.collisionWith(staticComponent);
            }
            if (this.turtle.isNearTo(staticComponent)) {
                this.turtle.collisionWith(staticComponent);
            }
        }
    }

    private void computeCoinCollisions() {
        List<ImageComponent> coins = getCoins();
        for (int i = 0; i < coins.size(); i++) {
            if (this.mario.collisionWithCoin(coins.get(i))) {
                util.Audio.play(Audio.Name.MONEY);
                coins.remove(i);
            }
        }
    }

    private void computeCharacterCollisions() {
        if (this.mushroom.isNearTo(turtle)) {
            this.mushroom.collisionWith(turtle);
        }
        if (this.turtle.isNearTo(mushroom)) {
            this.turtle.collisionWith(mushroom);
        }
        if (this.mario.isNearTo(mushroom)) {
            this.mario.collisionWith(mushroom);
        }
        if (this.mario.isNearTo(turtle)) {
            this.mario.collisionWith(turtle);
        }
    }

    @Override
    public void moveBackground() {
        Screen screen = getScreen();
        int mainXPosition = screen.getMainXPosition();
        int firstBackgroundXPosition = firstBackground.getX();
        int secondBackgroundXPosition = secondBackground.getX();
        int movingDelta = screen.getMovingDelta();
        if (screen.isInsideBounds()) {
            screen.setMainXPosition(mainXPosition + movingDelta);
            firstBackground.setX(firstBackgroundXPosition - movingDelta);
            secondBackground.setX(secondBackgroundXPosition - movingDelta);
        }
        firstBackgroundXPosition = firstBackground.getX();
        secondBackgroundXPosition = secondBackground.getX();
        if (firstBackgroundXPosition == Background.EXIT_SCREEN_POSITION) {
            firstBackground.setX(Background.ENTER_SCREEN_POSITION);
        } else if (secondBackgroundXPosition == Background.EXIT_SCREEN_POSITION) {
            secondBackground.setX(Background.ENTER_SCREEN_POSITION);
        } else if (firstBackgroundXPosition == Background.ENTER_SCREEN_POSITION) {
            firstBackground.setX(Background.EXIT_SCREEN_POSITION);
        } else if (secondBackgroundXPosition == Background.ENTER_SCREEN_POSITION) {
            secondBackground.setX(Background.EXIT_SCREEN_POSITION);
        }
    }

    @Override
    public void moveComponents() {
        List<ImageComponent> staticComponents = getStaticComponents();
        List<ImageComponent> coins = getCoins();
        Screen screen = getScreen();
        int mainXPosition = screen.getMainXPosition();
        int movingDelta = screen.getMovingDelta();
        if (screen.isInsideBounds()) {
            for (ImageComponent staticComponent : staticComponents) {
                staticComponent.move(mainXPosition, movingDelta);
            }
            for (ImageComponent coin : coins) {
                coin.move(mainXPosition, movingDelta);
            }
            this.mushroom.move(mainXPosition, movingDelta);
            this.turtle.move(mainXPosition, movingDelta);
        }
    }

    @Override
    public void moveMainCharacterRight() {
        Screen screen = getScreen();
        if (screen.isOverBoundsLeft()) {
            screen.setMainXPosition(Screen.START_POSITION);
            firstBackground.setX(ComponentConfiguration.DEFAULT_FIRST_BACKGROUND_X_POSITION);
            secondBackground.setX(ComponentConfiguration.DEFAULT_SECOND_BACKGROUND_X_POSITION);
        }
        this.mario.getCharacterState().setMoving(true);
        this.mario.getCharacterState().setTurnRight(true);
        screen.moveRight();
    }

    @Override
    public void moveMainCharacterLeft() {
        Screen screen = getScreen();
        if (screen.isOverBoundsRight()) {
            screen.setMainXPosition(Screen.END_POSITION);
            firstBackground.setX(ComponentConfiguration.DEFAULT_FIRST_BACKGROUND_X_POSITION);
            secondBackground.setX(ComponentConfiguration.DEFAULT_SECOND_BACKGROUND_X_POSITION);
        }
        this.mario.getCharacterState().setMoving(true);
        this.mario.getCharacterState().setTurnRight(false);
        screen.moveLeft();
    }

    @Override
    public void makeMainCharacterJump() {
        this.mario.setJumping(true);
        util.Audio.play(Audio.Name.JUMP);
    }

    @Override
    public void stopMainCharacter() {
        Screen screen = getScreen();
        this.mario.getCharacterState().setMoving(false);
        screen.stopMoving();
    }

}
