package game.component;

import game.component.character.MainCharacter;
import game.component.character.Mario;
import game.component.character.Mushroom;
import game.component.character.Turtle;
import game.component.object.Block;
import game.component.object.Coin;
import game.component.object.Tunnel;
import game.model.Position;
import game.model.Screen;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nicolalasagni on 04/03/2017.
 */
public class ComponentFactory {

    private static Block createBlock(Position position) {
        return new Block(position);
    }

    private static Coin createCoin(Position position) {
        return new Coin(position);
    }

    private static Tunnel createTunnel(Position position) {
        return new Tunnel(position);
    }

    public static MainCharacter createMainCharacter(Screen screen) {
        return new Mario(screen, new Position(300, 245));
    }

    public static Mushroom createMushroom() {
        return new Mushroom(new Position(800, 263));
    }

    public static Turtle createTurtle() {
        return new Turtle(new Position(950, 243));
    }

    private static List<Position> getBlocksPosition() {
        List<Position> positions = new ArrayList<>();
        positions.add(new Position(400, 180));
        positions.add(new Position(1200, 180));
        positions.add(new Position(1270, 170));
        positions.add(new Position(1340, 160));
        positions.add(new Position(2000, 180));
        positions.add(new Position(2600, 160));
        positions.add(new Position(2650, 180));
        positions.add(new Position(3500, 160));
        positions.add(new Position(3550, 140));
        positions.add(new Position(4000, 170));
        positions.add(new Position(4200, 200));
        positions.add(new Position(4300, 210));
        return positions;
    }

    private static List<Position> getCoinsPosition() {
        List<Position> positions = new ArrayList<>();
        positions.add(new Position(402, 145));
        positions.add(new Position(1202, 140));
        positions.add(new Position(1272, 95));
        positions.add(new Position(1342, 40));
        positions.add(new Position(1650, 145));
        positions.add(new Position(2650, 145));
        positions.add(new Position(3000, 135));
        positions.add(new Position(3400, 125));
        positions.add(new Position(4200, 145));
        positions.add(new Position(4600, 40));
        return positions;
    }

    private static List<Position> getTunnelPositions() {
        List<Position> positions = new ArrayList<>();
        positions.add(new Position(600, 230));
        positions.add(new Position(1000, 230));
        positions.add(new Position(1600, 230));
        positions.add(new Position(1900, 230));
        positions.add(new Position(2500, 230));
        positions.add(new Position(3000, 230));
        positions.add(new Position(3800, 230));
        positions.add(new Position(4500, 230));
        return positions;
    }

    private static List<Block> createBlocks() {
        List<Block> blocks = new ArrayList<>();
        List<Position> positions = getBlocksPosition();
        for (Position position : positions) {
            blocks.add(createBlock(position));
        }
        return blocks;
    }

    private static List<Tunnel> createTunnels() {
        List<Position> positions = getTunnelPositions();
        List<Tunnel> tunnels = new ArrayList<>();
        for (Position position : positions) {
            tunnels.add(createTunnel(position));
        }
        return tunnels;
    }

    public static List<ImageComponent> createStaticComponents() {
        List<ImageComponent> staticComponents = new ArrayList<>();
        staticComponents.addAll(createBlocks());
        staticComponents.addAll(createTunnels());
        return staticComponents;
    }

    public static List<ImageComponent> createCoins() {
        List<ImageComponent> coins = new ArrayList<>();
        List<Position> positions = getCoinsPosition();
        for (Position position : positions) {
            coins.add(createCoin(position));
        }
        return coins;
    }

}
