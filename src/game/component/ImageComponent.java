package game.component;

import game.model.Position;

import javax.swing.*;
import java.awt.*;

/**
 * Created by nicolalasagni on 04/03/2017.
 */
public class ImageComponent extends BaseComponent {

    public static final int ENTER_SCREEN_POSITION = 800;
    public static final int EXIT_SCREEN_POSITION = -800;

    private Image image;

    public ImageComponent(Position position, int width, int height, String imagePath) {
        super(position, width, height);
        ImageIcon imageIcon = new ImageIcon(getClass().getResource(imagePath));
        this.image = imageIcon.getImage();
    }

    public Image getImage() {
        return image;
    }

}
