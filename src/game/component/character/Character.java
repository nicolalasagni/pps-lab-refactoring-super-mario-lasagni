package game.component.character;

import game.component.BaseComponent;
import game.model.CharacterState;
import game.model.Position;
import util.Images;

import javax.swing.*;
import java.awt.*;

public abstract class Character extends BaseComponent {

    private static final int CONTACT_MARGIN = 5;
    private static final int NEAR_MARGIN = 10;

    private CharacterState characterState;
    protected Images.Character character;
    protected int setFrequency;

    Character(Images.Character character, int setFrequency, Position position, int width, int height) {
        super(position, width, height);
        this.characterState = new CharacterState();
        this.character = character;
        this.setFrequency = setFrequency;
    }

    public CharacterState getCharacterState() {
        return characterState;
    }

    public Image getAliveImage() {
        String imagePath;
        CharacterState state = getCharacterState();
        if (!state.isMoving()) {
            if (state.isTurnRight()) {
                imagePath = Images.getImagePath(character, Images.Direction.RIGHT, Images.State.NORMAL);
            } else {
                imagePath = Images.getImagePath(character, Images.Direction.LEFT, Images.State.NORMAL);
            }
        } else {
            state.incrementStepFrequency();
            if (state.getStepFrequency() / setFrequency == 0) { //
                if (state.isTurnRight()) {
                    imagePath = Images.getImagePath(character, Images.Direction.RIGHT, Images.State.NORMAL);
                } else
                    imagePath = Images.getImagePath(character, Images.Direction.LEFT, Images.State.NORMAL);
            } else {
                if (state.isTurnRight()) {
                    imagePath = Images.getImagePath(character, Images.Direction.RIGHT, Images.State.ACTIVE);
                } else
                    imagePath = Images.getImagePath(character, Images.Direction.LEFT, Images.State.ACTIVE);
            }
            if (state.getStepFrequency() == 2 * setFrequency)
                state.setStepFrequency(0);
        }

        ImageIcon imageIcon = new ImageIcon(getClass().getResource(imagePath));
        return imageIcon.getImage();
    }

    boolean rightCollision(BaseComponent baseComponent) {
        int currentXPosition = getX() + getWidth();
        int objectXPosition = baseComponent.getX();
        return !(currentXPosition < objectXPosition ||
                currentXPosition > objectXPosition + CONTACT_MARGIN ||
                getY() + getHeight() <= baseComponent.getY() ||
                getY() >= baseComponent.getY() + baseComponent.getHeight());
    }

    boolean leftCollision(BaseComponent baseComponent) {
        int xPosition = getX();
        int yPosition = getY();
        return !(xPosition > baseComponent.getX() + baseComponent.getWidth() ||
                xPosition + getWidth() < baseComponent.getX() + baseComponent.getWidth() - CONTACT_MARGIN ||
                yPosition + getHeight() <= baseComponent.getY() ||
                yPosition >= baseComponent.getY() + baseComponent.getHeight());
    }

    boolean downCollision(BaseComponent baseComponent) {
        int xPosition = getX();
        int yPosition = getY();
        return !(xPosition + getWidth() < baseComponent.getX() + CONTACT_MARGIN ||
                xPosition > baseComponent.getX() + baseComponent.getWidth() - CONTACT_MARGIN ||
                yPosition + getHeight() < baseComponent.getY() ||
                yPosition + getHeight() > baseComponent.getY() + CONTACT_MARGIN);
    }
    
    boolean upCollision(BaseComponent baseComponent) {
        int xPosition = getX();
        int yPosition = getY();
        return !(xPosition + getWidth() < baseComponent.getX() + CONTACT_MARGIN ||
                xPosition > baseComponent.getX() + baseComponent.getWidth() - CONTACT_MARGIN ||
                yPosition < baseComponent.getY() + baseComponent.getHeight() ||
                yPosition > baseComponent.getY() + baseComponent.getHeight() + CONTACT_MARGIN);
    }

    public boolean isNearTo(BaseComponent baseComponent) {
        int xPosition = getX();
        return (xPosition > baseComponent.getX() - NEAR_MARGIN && xPosition < baseComponent.getX() + baseComponent.getWidth() + NEAR_MARGIN) ||
                (xPosition + getWidth() > baseComponent.getX() - NEAR_MARGIN && xPosition + getWidth() < baseComponent.getX() + baseComponent.getWidth() + NEAR_MARGIN);

    }

}
