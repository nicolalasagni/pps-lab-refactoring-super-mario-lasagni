package game.component.character;

import game.component.BaseComponent;
import game.component.ImageComponent;
import game.model.CharacterState;
import game.model.Position;
import game.model.Screen;
import util.Images;

import java.awt.*;

/**
 * Created by nicolalasagni on 04/03/2017.
 */
public abstract class MainCharacter extends Character {

    protected Screen screen;
    protected boolean jumping;
    protected int jumpDepth;

    MainCharacter(Images.Character character, int frequency, Screen screen, Position position, int width, int height) {
        super(character, frequency, position, width, height);
        this.screen = screen;
        this.jumping = false;
        this.jumpDepth = 0;
    }

    public boolean isJumping() {
        return jumping;
    }

    public void setJumping(boolean jumping) {
        this.jumping = jumping;
    }

    public int getJumpDepth() {
        return jumpDepth;
    }

    public void incrementJumpDepth() {
        jumpDepth++;
    }

    public void setJumpDepth(int jumpDepth) {
        this.jumpDepth = jumpDepth;
    }

    public boolean collisionWithCoin(ImageComponent coin) {
        return this.leftCollision(coin) || this.upCollision(coin) || this.rightCollision(coin) || this.downCollision(coin);
    }

    public abstract Image jump();

    public void collisionWith(Character character) {
        CharacterState characterState = character.getCharacterState();
        if (rightCollision(character) || leftCollision(character)) {
            if (characterState.isAlive()) {
                getCharacterState().setMoving(false);
                getCharacterState().setAlive(false);
            } else {
                getCharacterState().setAlive(true);
            }
        } else if (downCollision(character)) {
            characterState.setMoving(false);
            characterState.setAlive(false);
        }
    }

    public void collisionWith(BaseComponent baseComponent) {
        CharacterState characterState = getCharacterState();
        if (rightCollision(baseComponent) && characterState.isTurnRight() ||
                (leftCollision(baseComponent)) && !characterState.isTurnRight()) {
            this.screen.stopMoving();
            characterState.setMoving(false);
        }
        if (downCollision(baseComponent) && isJumping()) {
            this.screen.setFloorY(baseComponent.getY());
        } else if (!downCollision(baseComponent)) {
            this.screen.setFloorY(293);
            if (!isJumping()) {
                setPosition(getX(), 243);
            }
            if (upCollision(baseComponent)) {
                this.screen.setTopHeight(baseComponent.getY() + baseComponent.getHeight());
            } else if (!upCollision(baseComponent) && !isJumping()) {
                this.screen.setTopHeight(0);
            }
        }
    }

}
