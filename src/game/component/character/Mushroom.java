package game.component.character;

import game.model.CharacterState;
import game.model.Position;
import util.Images;

import javax.swing.*;
import java.awt.*;

public class Mushroom extends EnemyCharacter implements Runnable {

    private static final String TAG = Mushroom.class.getSimpleName();

    private static final int STEP_FREQUENCY = 45;
    private static final int WIDTH = 27;
    private static final int HEIGHT = 30;

    public Mushroom(Position position) {
        super(Images.Character.MUSHROOM, STEP_FREQUENCY, TAG, position, WIDTH, HEIGHT);
    }

    @Override
    public Image getDeadImage() {
        CharacterState characterState = getCharacterState();
        String imagePath = characterState.isTurnRight() ? Images.MUSHROOM_RIGHT_DEAD : Images.MUSHROOM_LEFT_DEAD;
        ImageIcon imageIcon = new ImageIcon(getClass().getResource(imagePath));
        return imageIcon.getImage();
    }

    @Override
    public int getDeadY() {
        return this.getY() + 20;
    }

}
