package game.component.character;

import game.model.Position;
import util.Images;

import javax.swing.*;
import java.awt.*;

public class Turtle extends EnemyCharacter implements Runnable {

    private static final String TAG = Turtle.class.getSimpleName();

    private static final int STEP_FREQUENCY = 45;
    private static final int WIDTH = 43;
    private static final int HEIGHT = 50;

    public Turtle(Position position) {
        super(Images.Character.TURTLE, STEP_FREQUENCY, TAG, position, WIDTH, HEIGHT);
    }

    @Override
    public Image getDeadImage() {
        String imagePath = Images.TURTLE_DEAD;
        ImageIcon imageIcon = new ImageIcon(getClass().getResource(imagePath));
        return imageIcon.getImage();
    }

    @Override
    public int getDeadY() {
        return this.getY() + 30;
    }

}
