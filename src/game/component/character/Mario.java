package game.component.character;

import game.model.CharacterState;
import game.model.Position;
import game.model.Screen;
import util.Images;

import javax.swing.*;
import java.awt.*;

public class Mario extends MainCharacter {

    private static final int STEP_FREQUENCY = 25;
    private static final int WIDTH = 28;
    private static final int HEIGHT = 50;

    public Mario(Screen screen, Position position) {
        super(Images.Character.MARIO, STEP_FREQUENCY, screen, position, WIDTH, HEIGHT);
    }

    @Override
    public Image getAliveImage() {
        String imagePath;
        CharacterState characterState = getCharacterState();
        if (!characterState.isMoving() ||
                this.screen.getMainXPosition() <= 0 ||
                this.screen.getMainXPosition() > 4600) {
            if (characterState.isTurnRight()) {
                imagePath = Images.getImagePath(character, Images.Direction.RIGHT, Images.State.NORMAL);
            } else {
                imagePath = Images.getImagePath(character, Images.Direction.LEFT, Images.State.NORMAL);
            }
        } else {
            characterState.incrementStepFrequency();
            if (characterState.getStepFrequency() / setFrequency == 0) { //
                if (characterState.isTurnRight()) {
                    imagePath = Images.getImagePath(character, Images.Direction.RIGHT, Images.State.NORMAL);
                } else {
                    imagePath = Images.getImagePath(character, Images.Direction.LEFT, Images.State.NORMAL);
                }
            } else {
                if (characterState.isTurnRight()) {
                    imagePath = Images.getImagePath(character, Images.Direction.RIGHT, Images.State.ACTIVE);
                } else {
                    imagePath = Images.getImagePath(character, Images.Direction.LEFT, Images.State.ACTIVE);
                }
            }
            if (characterState.getStepFrequency() == 2 * setFrequency)
                characterState.setStepFrequency(0);
        }

        ImageIcon imageIcon = new ImageIcon(getClass().getResource(imagePath));
        return imageIcon.getImage();

    }

    public Image jump() {
        CharacterState characterState = getCharacterState();
        ImageIcon ico;
        Image img;
        String str;
        incrementJumpDepth();
        int jumpDepth = getJumpDepth();
        if (jumpDepth <= 41) {
            if (this.getY() > this.screen.getTopHeight()) {
                setPosition(getX(), getY() - 4);
            } else {
                setJumpDepth(42);
            }
            if (characterState.isTurnRight()) {
                str = Images.MARIO_RIGHT_JUMP;
            } else {
                str = Images.MARIO_LEFT_JUMP;
            }
        } else if (this.getY() + this.getHeight() < this.screen.getFloorY()) {
            setPosition(getX(), getY() + 1);
            if (characterState.isTurnRight()) {
                str = Images.MARIO_RIGHT_JUMP;
            } else {
                str = Images.MARIO_LEFT_JUMP;
            }
        } else {
            if (characterState.isTurnRight()) {
                str = Images.MARIO_RIGHT_NORMAL;
            } else {
                str = Images.MARIO_LEFT_NORMAL;
            }
            setJumping(false);
            setJumpDepth(0);
        }
        ico = new ImageIcon(getClass().getResource(str));
        img = ico.getImage();
        return img;
    }

}
