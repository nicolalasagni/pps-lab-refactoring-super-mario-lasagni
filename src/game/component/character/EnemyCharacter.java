package game.component.character;

import game.component.BaseComponent;
import game.model.CharacterState;
import game.model.Position;
import util.Images;
import util.Logger;

import java.awt.*;

/**
 * Created by nicolalasagni on 04/03/2017.
 */
public abstract class EnemyCharacter extends Character implements Runnable {

    private static final int PAUSE = 15;

    private String name;
    private int movementDelta;

    EnemyCharacter(Images.Character character, int frequency, String name, Position position, int width, int height) {
        super(character, frequency, position, width, height);
        this.name = name;
        this.movementDelta = 1;
        getCharacterState().setTurnRight(true);
        getCharacterState().setMoving(true);
        new Thread(this).start();
    }

    public abstract Image getDeadImage();

    public abstract int getDeadY();

    private void move() {
        if (getCharacterState().isTurnRight()) {
            this.movementDelta = 1;
        } else {
            this.movementDelta = -1;
        }
        setPosition(getX() + this.movementDelta, getY());

    }

    public void collisionWith(BaseComponent obj) {
        CharacterState characterState = getCharacterState();
        if (rightCollision(obj) && characterState.isTurnRight()) {
            characterState.setTurnRight(false);
            this.movementDelta = -1;
        } else if (leftCollision(obj) && !characterState.isTurnRight()) {
            characterState.setTurnRight(true);
            this.movementDelta = 1;
        }
    }

    @Override
    public void run() {
        try {
            Thread.sleep(20);
        } catch (InterruptedException e) {
            Logger.print(name, e);
        }
        while (true) {
            if (getCharacterState().isAlive()) {
                this.move();
                try {
                    Thread.sleep(PAUSE);
                } catch (InterruptedException e) {
                    Logger.print(name, e);
                }
            }
        }
    }
}
