package game.component.object;

import game.component.ImageComponent;
import game.model.Counter;
import game.model.Position;
import util.Images;

import javax.swing.*;
import java.awt.*;

public class Coin extends ImageComponent {

    private static final int WIDTH = 30;
    private static final int HEIGHT = 30;
    private static final int CHANGE_IMAGE_THRESHOLD = 100;
    private static final int RESET_COUNTER_THRESHOLD = 200;

    private Counter counter;

    public Coin(Position position) {
        super(position, WIDTH, HEIGHT, Images.COIN);
        this.counter = new Counter();
    }

    @Override
    public Image getImage() {
        this.counter.incrementCounter();
        int counter = this.counter.getCounter();
        String imagePath = getImagePath(counter);
        if (counter == RESET_COUNTER_THRESHOLD) {
            this.counter.resetCounter();
        }
        ImageIcon imageIcon = new ImageIcon(getClass().getResource(imagePath));
        return imageIcon.getImage();
    }

    private String getImagePath(int counter) {
        if (counter / CHANGE_IMAGE_THRESHOLD == 0) {
            return Images.COIN;
        } else {
            return Images.COIN_DISABLED;
        }
    }

}
