package game.component.object;

import game.component.ImageComponent;
import game.model.Position;
import util.Images;

public class Tunnel extends ImageComponent {

    private static final int WIDTH = 43;
    private static final int HEIGHT = 65;

    public Tunnel(Position position) {
        super(position, WIDTH, HEIGHT, Images.TUNNEL);
    }

}
