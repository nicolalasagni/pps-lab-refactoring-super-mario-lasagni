package game.component.object;

import game.component.ImageComponent;
import game.model.Position;
import util.Images;

public class Block extends ImageComponent {

    private static final int WIDTH = 30;
    private static final int HEIGHT = 30;

    public Block(Position position) {
            super(position, WIDTH, HEIGHT, Images.BLOCK);
    }

}
