package game.component.object;

import game.component.ImageComponent;
import game.model.Position;
import util.Images;

/**
 * Created by nicolalasagni on 04/03/2017.
 */
public class Background extends ImageComponent {

    public Background(Position position) {
        super(position, 0, 0, Images.BACKGROUND);
    }

}
