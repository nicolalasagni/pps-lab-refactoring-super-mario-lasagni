package game.component.object;

import game.component.ImageComponent;
import game.model.Position;
import util.Images;

/**
 * Created by nicolalasagni on 04/03/2017.
 */
public class Castle extends ImageComponent {

    private static final int X_POSITION = 4850;
    private static final int Y_POSITION = 145;

    public Castle() {
        super(new Position(X_POSITION, Y_POSITION), 0, 0, Images.CASTLE);
    }

}
