package game.component;

import game.model.Position;

public class BaseComponent {

    private int width;
    private int height;
    private Position position;

    public BaseComponent(Position position, int width, int height) {
        this.position = position;
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Position getPosition() {
        return position;
    }

    protected void setPosition(int x, int y) {
        this.position = new Position(x, y);
    }

    public int getX() {
        Position position = getPosition();
        return position.getX();
    }

    public void setX(int x) {
        setPosition(x, getY());
    }

    public int getY() {
        Position position = getPosition();
        return position.getY();
    }

    public void setY(int y) {
        setPosition(getX(), y);
    }

    public void move(int position, int movement) {
        if (position >= 0) {
            int x = getX() - movement;
            int y = getY();
            setPosition(x, y);
        }
    }

}
