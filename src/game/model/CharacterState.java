package game.model;

/**
 * Created by nicolalasagni on 04/03/2017.
 */
public class CharacterState {

    private boolean moving;
    private int stepFrequency;
    private boolean turnRight = true;
    private boolean alive = true;

    public boolean isMoving() {
        return moving;
    }

    public void setMoving(boolean moving) {
        this.moving = moving;
    }

    public int getStepFrequency() {
        return stepFrequency;
    }

    public void incrementStepFrequency() {
        this.stepFrequency++;
    }

    public void setStepFrequency(int stepFrequency) {
        this.stepFrequency = stepFrequency;
    }

    public boolean isTurnRight() {
        return turnRight;
    }

    public void setTurnRight(boolean turnRight) {
        this.turnRight = turnRight;
    }

    public boolean isAlive() {
        return alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

}
