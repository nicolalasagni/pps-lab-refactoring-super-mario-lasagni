package game.model;

/**
 * Created by nicolalasagni on 03/03/2017.
 */
public class ViewConfiguration {

    private String title;
    private int viewWidth;
    private int viewHeight;

    public ViewConfiguration(String title, int viewWidth, int viewHeight) {
        this.title = title;
        this.viewWidth = viewWidth;
        this.viewHeight = viewHeight;
    }

    public String getTitle() {
        return title;
    }

    public int getViewWidth() {
        return viewWidth;
    }

    public int getViewHeight() {
        return viewHeight;
    }

}
