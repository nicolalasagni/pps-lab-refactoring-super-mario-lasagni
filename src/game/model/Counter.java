package game.model;

/**
 * Created by nicolalasagni on 04/03/2017.
 */
public class Counter {

    private int counter;

    public int getCounter() {
        return this.counter;
    }

    public void incrementCounter() {
        this.counter++;
    }

    public void resetCounter() {
        this.counter = 0;
    }

}
