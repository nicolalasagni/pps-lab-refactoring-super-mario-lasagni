package game.model;

import game.component.ImageComponent;
import game.component.character.EnemyCharacter;
import game.component.character.MainCharacter;
import game.component.object.Background;
import game.component.object.Castle;
import game.component.object.Flag;
import game.component.object.Start;

import java.util.List;

/**
 * Created by nicolalasagni on 04/03/2017.
 */
public class ComponentConfiguration {

    public static final int DEFAULT_FIRST_BACKGROUND_X_POSITION = -50;
    public static final int DEFAULT_SECOND_BACKGROUND_X_POSITION = 750;

    private ImageComponent firstBackground;
    private ImageComponent secondBackground;
    private ImageComponent castle;
    private ImageComponent flag;
    private ImageComponent start;
    private MainCharacter mainCharacter;
    private EnemyCharacter mushroom;
    private EnemyCharacter turtle;
    private List<ImageComponent> staticComponents;
    private List<ImageComponent> coins;

    public ComponentConfiguration(MainCharacter mainCharacter,
                                  EnemyCharacter mushroom,
                                  EnemyCharacter turtle,
                                  List<ImageComponent> staticComponents,
                                  List<ImageComponent> coins) {
        this.mainCharacter = mainCharacter;
        this.mushroom = mushroom;
        this.turtle = turtle;
        this.staticComponents = staticComponents;
        this.coins = coins;
        initBackgroundComponents();
    }

    private void initBackgroundComponents() {
        Position firstPosition = new Position(DEFAULT_FIRST_BACKGROUND_X_POSITION, 0);
        this.firstBackground = new Background(firstPosition);
        Position secondPosition = new Position(DEFAULT_SECOND_BACKGROUND_X_POSITION, 0);
        this.secondBackground = new Background(secondPosition);
        this.castle = new Castle();
        this.flag = new Flag();
        this.start = new Start();
    }

    public ImageComponent getCastle() {
        return castle;
    }

    public ImageComponent getFlag() {
        return flag;
    }

    public ImageComponent getStart() {
        return start;
    }

    public ImageComponent getFirstBackground() {
        return firstBackground;
    }

    public ImageComponent getSecondBackground() {
        return secondBackground;
    }

    public MainCharacter getMainCharacter() {
        return mainCharacter;
    }

    public EnemyCharacter getMushroom() {
        return mushroom;
    }

    public EnemyCharacter getTurtle() {
        return turtle;
    }

    public List<ImageComponent> getStaticComponents() {
        return staticComponents;
    }

    public List<ImageComponent> getCoins() {
        return coins;
    }

}
