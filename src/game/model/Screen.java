package game.model;

/**
 * Created by nicolalasagni on 04/03/2017.
 */
public class Screen {

    public static final int  START_POSITION = 0;
    public static final int  END_POSITION = 4600;

    private static final int DEFAULT_MOVING_DELTA = 0;
    private static final int DEFAULT_MAIN_X_POSITION = -1;
    private static final int DEFAULT_FLOOR_Y = 293;
    private static final int DEFAULT_TOP_HEIGHT = 0;

    private static final int MOVE_DELTA_RIGHT = 1;
    private static final int MOVE_DELTA_LEFT = -1;
    private static final int STOP_MOVING = 0;

    private int movingDelta;
    private int mainXPosition;
    private int floorY;
    private int topHeight;

    public Screen() {
        this.movingDelta = DEFAULT_MOVING_DELTA;
        this.mainXPosition = DEFAULT_MAIN_X_POSITION;
        this.floorY = DEFAULT_FLOOR_Y;
        this.topHeight = DEFAULT_TOP_HEIGHT;
    }

    public int getMovingDelta() {
        return movingDelta;
    }

    public void moveRight() {
        this.movingDelta = MOVE_DELTA_RIGHT;
    }

    public void moveLeft() {
        this.movingDelta = MOVE_DELTA_LEFT;
    }

    public void stopMoving() {
        this.movingDelta = STOP_MOVING;
    }

    public boolean isInsideBounds() {
        return mainXPosition >= START_POSITION && mainXPosition <= END_POSITION;
    }

    public boolean isOverBoundsLeft() {
        return mainXPosition == START_POSITION - 1;
    }

    public boolean isOverBoundsRight() {
        return mainXPosition == END_POSITION + 1;
    }

    public int getMainXPosition() {
        return mainXPosition;
    }

    public void setMainXPosition(int mainXPosition) {
        this.mainXPosition = mainXPosition;
    }

    public int getFloorY() {
        return floorY;
    }

    public void setFloorY(int floorY) {
        this.floorY = floorY;
    }

    public int getTopHeight() {
        return topHeight;
    }

    public void setTopHeight(int topHeight) {
        this.topHeight = topHeight;
    }

}
