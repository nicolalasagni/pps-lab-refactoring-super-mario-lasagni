package game.builder;

import game.component.ComponentFactory;
import game.component.ImageComponent;
import game.component.character.MainCharacter;
import game.component.character.Mushroom;
import game.component.character.Turtle;
import game.controller.GameController;
import game.controller.GameControllerImpl;
import game.model.ComponentConfiguration;
import game.model.Screen;
import game.model.ViewConfiguration;
import game.view.GameViewImpl;
import game.view.GameViewUpdater;
import game.view.SimpleGameView;

import javax.swing.*;
import java.util.List;

/**
 * Created by nicolalasagni on 03/03/2017.
 */
public class GameBuilder {

    private ViewConfiguration viewConfiguration;

    public GameBuilder(ViewConfiguration viewConfiguration) {
        this.viewConfiguration = viewConfiguration;
    }

    public void startGame() {
        Screen screen = buildScreen();
        ComponentConfiguration componentConfiguration = buildComponentConfiguration(screen);
        SimpleGameView gameView = buildGameView();
        GameController gameController = new GameControllerImpl(gameView, screen, componentConfiguration);
        gameView.setController(gameController);
        startGameUpdater(gameView);
    }

    private Screen buildScreen() {
        return new Screen();
    }

    private ComponentConfiguration buildComponentConfiguration(Screen screen) {
        MainCharacter mainCharacter = ComponentFactory.createMainCharacter(screen);
        Mushroom mushroom = ComponentFactory.createMushroom();
        Turtle turtle = ComponentFactory.createTurtle();
        List<ImageComponent> staticComponents = ComponentFactory.createStaticComponents();
        List<ImageComponent> coins = ComponentFactory.createCoins();
        return new ComponentConfiguration(mainCharacter, mushroom, turtle, staticComponents, coins);
    }

    private SimpleGameView buildGameView() {
        SimpleGameView gameView = new GameViewImpl();
        buildGameFrame(gameView);
        return gameView;
    }

    private JFrame buildGameFrame(JPanel jPanel) {
        String title = this.viewConfiguration.getTitle();
        int viewWidth = this.viewConfiguration.getViewWidth();
        int viewHeight = this.viewConfiguration.getViewHeight();
        JFrame jFrame = new JFrame(title);
        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jFrame.setSize(viewWidth, viewHeight);
        jFrame.setLocationRelativeTo(null);
        jFrame.setResizable(true);
        jFrame.setAlwaysOnTop(true);
        jFrame.setContentPane(jPanel);
        jFrame.setVisible(true);
        return jFrame;
    }

    private void startGameUpdater(JPanel jPanel) {
        Thread gameUpdater = new Thread(new GameViewUpdater(jPanel));
        gameUpdater.start();
    }

}
