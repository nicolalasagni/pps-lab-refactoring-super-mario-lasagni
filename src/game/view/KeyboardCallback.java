package game.view;

/**
 * Created by nicolalasagni on 04/03/2017.
 */
public interface KeyboardCallback {

    enum ButtonType {
        NONE,
        UP,
        RIGHT,
        LEFT
    }

    void onButtonPressed(ButtonType buttonType);

    void onButtonsReleased();

}
