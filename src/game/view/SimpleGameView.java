package game.view;

import game.controller.GameController;

import javax.swing.*;

/**
 * Created by nicolalasagni on 04/03/2017.
 */
public abstract class SimpleGameView extends JPanel implements GameView {

    private GameController gameController;

    SimpleGameView() {
        super();
    }

    @Override
    public void setKeyListener(KeyboardListener keyboardListener) {
        this.addKeyListener(keyboardListener);
    }

    @Override
    public void setController(GameController controller) {
        this.gameController = controller;
    }

    void notifyController() {
        if (this.gameController != null) {
            this.gameController.updateGame();
        }
    }

}
