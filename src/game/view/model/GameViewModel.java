package game.view.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nicolalasagni on 04/03/2017.
 */
public class GameViewModel {

    private List<GameImageViewModel> imageViewModels = new ArrayList<>();

    public List<GameImageViewModel> getImageViewModels() {
        return imageViewModels;
    }

    public void setImageViewModels(List<GameImageViewModel> imageViewModels) {
        this.imageViewModels = imageViewModels;
    }

}
