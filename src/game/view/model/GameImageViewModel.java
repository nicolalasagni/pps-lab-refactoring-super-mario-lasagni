package game.view.model;

import game.model.Position;

import java.awt.*;

/**
 * Created by nicolalasagni on 04/03/2017.
 */
public class GameImageViewModel {

    private Image image;
    private Position position;

    public GameImageViewModel(Image image, Position position) {
        this.image = image;
        this.position = position;
    }

    public Image getImage() {
        return image;
    }

    public Position getPosition() {
        return position;
    }
}
