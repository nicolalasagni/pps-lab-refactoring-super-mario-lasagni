package game.view;

import util.Logger;

import javax.swing.*;

public class GameViewUpdater implements Runnable {

    private static final String TAG = GameViewUpdater.class.getSimpleName();
    private static final int PAUSE_IN_MILLIS = 3;

    private JPanel jPanel;

    public GameViewUpdater(JPanel jPanel) {
        this.jPanel = jPanel;
    }

    public void run() {
        while (true) {
            this.jPanel.repaint();
            try {
                Thread.sleep(PAUSE_IN_MILLIS);
            } catch (InterruptedException e) {
                Logger.print(TAG, e);
            }
        }
    }

} 
