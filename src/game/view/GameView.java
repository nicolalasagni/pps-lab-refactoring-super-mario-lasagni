package game.view;

import game.controller.GameController;
import game.view.model.GameViewModel;

/**
 * Created by nicolalasagni on 04/03/2017.
 */
public interface GameView {

    String TAG = GameView.class.getSimpleName();

    void setController(GameController controller);

    void setKeyListener(KeyboardListener keyboardListener);

    void renderViewModel(GameViewModel viewModel);

}
