package game.view;

import game.model.Position;
import game.view.model.GameImageViewModel;
import game.view.model.GameViewModel;
import util.Logger;

import java.awt.*;
import java.util.List;


public class GameViewImpl extends SimpleGameView {

    private static final String RENDERING_ERROR = "Image rendering failed due to null graphics!";

    private Graphics graphics;

    public GameViewImpl() {
        super();
        this.setFocusable(true);
        this.requestFocusInWindow();
    }

    public void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        this.graphics = graphics;
        notifyController();
    }

    private void renderImage(Image image, int xPosition, int yPosition) {
        if (this.graphics != null) {
            this.graphics.drawImage(image, xPosition, yPosition, null);
        } else {
            Logger.print(TAG, RENDERING_ERROR);
        }
    }

    @Override
    public void renderViewModel(GameViewModel viewModel) {
        List<GameImageViewModel> imageViewModels = viewModel.getImageViewModels();
        for (GameImageViewModel imageViewModel : imageViewModels) {
            Position position = imageViewModel.getPosition();
            renderImage(imageViewModel.getImage(), position.getX(), position.getY());
        }
    }

}
