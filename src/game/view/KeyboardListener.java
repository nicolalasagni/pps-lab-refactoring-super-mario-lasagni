package game.view;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyboardListener implements KeyListener {

    private KeyboardCallback callback;

    public KeyboardListener(KeyboardCallback callback) {
        this.callback = callback;
    }

    @Override
    public void keyPressed(KeyEvent e) {
        KeyboardCallback.ButtonType buttonType = getButtonType(e);
        this.callback.onButtonPressed(buttonType);
    }

    private KeyboardCallback.ButtonType getButtonType(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_RIGHT:
                return KeyboardCallback.ButtonType.RIGHT;
            case KeyEvent.VK_LEFT:
                return KeyboardCallback.ButtonType.LEFT;
            case KeyEvent.VK_UP:
                return KeyboardCallback.ButtonType.UP;
        }

        return KeyboardCallback.ButtonType.NONE;
    }

    @Override
    public void keyReleased(KeyEvent e) {
        this.callback.onButtonsReleased();
    }

    @Override
    public void keyTyped(KeyEvent e) {}

}
