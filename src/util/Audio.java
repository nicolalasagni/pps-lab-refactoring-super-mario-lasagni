package util;

import javax.sound.sampled.*;
import java.io.IOException;

/**
 * Created by nicolalasagni on 03/03/2017.
 */
public class Audio {

    private static final String TAG = Audio.class.getSimpleName();
    private static final String ERROR = "Error occurred while retrieving audio resource!";

    private static final String BASE_PATH = "audio/";
    private static final String AUDIO_EXTENSION = ".wav";

    private static final String JUMP = Resources.BASE_PATH + BASE_PATH + "jump" + AUDIO_EXTENSION;
    private static final String MONEY = Resources.BASE_PATH + BASE_PATH + "money" + AUDIO_EXTENSION;

    public enum Name {
        JUMP,
        MONEY
    }

    public static void play(Name audioName) {
        String audioPath = getAudioPath(audioName);
        try {
            AudioInputStream audioInputStream = getAudioInputStream(audioPath);
            Clip clip = getClip();
            clip.open(audioInputStream);
            clip.start();
        } catch (UnsupportedAudioFileException | LineUnavailableException | IOException e) {
            Logger.print(TAG, ERROR, e);
        }
    }

    private static String getAudioPath(Name audioName) {
        switch (audioName) {
            case JUMP:
                return JUMP;
            case MONEY:
                return MONEY;
            default:
                return "";
        }
    }

    private static AudioInputStream getAudioInputStream(String audioPath) throws IOException, UnsupportedAudioFileException {
        return AudioSystem.getAudioInputStream(util.Audio.class.getClass().getResource(audioPath));
    }

    private static Clip getClip() throws LineUnavailableException {
        return AudioSystem.getClip();
    }

}
