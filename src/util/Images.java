package util;

/**
 * Created by nicolalasagni on 03/03/2017.
 */
public class Images {

    private static final String BASE_PATH = "images/";
    private static final String IMAGE_EXTENSION = ".png";
    private static final String SEPARATOR = "_";

    private static final String OBJECT_BACKGROUND = "background";
    private static final String OBJECT_BLOCK = "block";
    private static final String OBJECT_CASTLE = "castle";
    private static final String OBJECT_COIN = "coin";
    private static final String OBJECT_FLAG = "flag";
    private static final String OBJECT_START = "start";
    private static final String OBJECT_TUNNEL = "tunnel";

    private static final String CHARACTER_MUSHROOM = "mushroom";
    private static final String CHARACTER_TURTLE = "turtle";
    private static final String CHARACTER_MARIO = "mario";

    private static final String DIRECTION_RIGHT = "right";
    private static final String DIRECTION_LEFT = "left";

    private static final String STATE_ACTIVE = "active";
    private static final String STATE_JUMP = "jump";
    private static final String STATE_NORMAL = "normal";
    private static final String STATE_DEAD = "dead";
    private static final String STATE_DISABLED = "disabled";
    private static final String STATE_BIG = "big";

    public static final String BACKGROUND = Resources.BASE_PATH + BASE_PATH + OBJECT_BACKGROUND + IMAGE_EXTENSION;
    public static final String BLOCK = Resources.BASE_PATH + BASE_PATH + OBJECT_BLOCK + IMAGE_EXTENSION;
    public static final String CASTLE = Resources.BASE_PATH + BASE_PATH + OBJECT_CASTLE + IMAGE_EXTENSION;
    public static final String CASTLE_BIG = Resources.BASE_PATH + BASE_PATH + OBJECT_CASTLE + SEPARATOR + STATE_BIG + IMAGE_EXTENSION;
    public static final String COIN_DISABLED = Resources.BASE_PATH + BASE_PATH + OBJECT_COIN + SEPARATOR + STATE_DISABLED + IMAGE_EXTENSION;
    public static final String COIN = Resources.BASE_PATH + BASE_PATH + OBJECT_COIN + IMAGE_EXTENSION;
    public static final String FLAG = Resources.BASE_PATH + BASE_PATH + OBJECT_FLAG + IMAGE_EXTENSION;
    public static final String START = Resources.BASE_PATH + BASE_PATH + OBJECT_START + IMAGE_EXTENSION;
    public static final String TUNNEL = Resources.BASE_PATH + BASE_PATH + OBJECT_TUNNEL + IMAGE_EXTENSION;

    public static final String MARIO_LEFT_ACTIVE = Resources.BASE_PATH + BASE_PATH + CHARACTER_MARIO + SEPARATOR + DIRECTION_LEFT + SEPARATOR + STATE_ACTIVE + IMAGE_EXTENSION;
    public static final String MARIO_LEFT_JUMP = Resources.BASE_PATH + BASE_PATH + CHARACTER_MARIO + SEPARATOR + DIRECTION_LEFT + SEPARATOR + STATE_JUMP + IMAGE_EXTENSION;
    public static final String MARIO_LEFT_NORMAL = Resources.BASE_PATH + BASE_PATH + CHARACTER_MARIO + SEPARATOR + DIRECTION_LEFT + SEPARATOR + STATE_NORMAL + IMAGE_EXTENSION;
    public static final String MARIO_RIGHT_ACTIVE = Resources.BASE_PATH + BASE_PATH + CHARACTER_MARIO + SEPARATOR + DIRECTION_RIGHT + SEPARATOR + STATE_ACTIVE + IMAGE_EXTENSION;
    public static final String MARIO_RIGHT_JUMP = Resources.BASE_PATH + BASE_PATH + CHARACTER_MARIO + SEPARATOR + DIRECTION_RIGHT + SEPARATOR + STATE_JUMP + IMAGE_EXTENSION;
    public static final String MARIO_RIGHT_NORMAL = Resources.BASE_PATH + BASE_PATH + CHARACTER_MARIO + SEPARATOR + DIRECTION_RIGHT + SEPARATOR + STATE_NORMAL + IMAGE_EXTENSION;

    public static final String MUSHROOM_LEFT_ACTIVE = Resources.BASE_PATH + BASE_PATH + CHARACTER_MUSHROOM + SEPARATOR + DIRECTION_LEFT + SEPARATOR + STATE_ACTIVE + IMAGE_EXTENSION;
    public static final String MUSHROOM_LEFT_DEAD = Resources.BASE_PATH + BASE_PATH + CHARACTER_MUSHROOM + SEPARATOR + DIRECTION_LEFT + SEPARATOR + STATE_DEAD + IMAGE_EXTENSION;
    public static final String MUSHROOM_LEFT_NORMAL = Resources.BASE_PATH + BASE_PATH + CHARACTER_MUSHROOM + SEPARATOR + DIRECTION_LEFT + SEPARATOR + STATE_NORMAL + IMAGE_EXTENSION;
    public static final String MUSHROOM_RIGHT_ACTIVE = Resources.BASE_PATH + BASE_PATH + CHARACTER_MUSHROOM + SEPARATOR + DIRECTION_RIGHT + SEPARATOR + STATE_ACTIVE + IMAGE_EXTENSION;
    public static final String MUSHROOM_RIGHT_DEAD = Resources.BASE_PATH + BASE_PATH + CHARACTER_MUSHROOM + SEPARATOR + DIRECTION_RIGHT + SEPARATOR + STATE_DEAD + IMAGE_EXTENSION;
    public static final String MUSHROOM_RIGHT_NORMAL = Resources.BASE_PATH + BASE_PATH + CHARACTER_MUSHROOM + SEPARATOR + DIRECTION_RIGHT + SEPARATOR + STATE_NORMAL + IMAGE_EXTENSION;

    public static final String TURTLE_DEAD = Resources.BASE_PATH + BASE_PATH + CHARACTER_TURTLE + SEPARATOR + STATE_DEAD + IMAGE_EXTENSION;
    public static final String TURTLE_LEFT_ACTIVE = Resources.BASE_PATH + BASE_PATH + CHARACTER_TURTLE + SEPARATOR + DIRECTION_LEFT + SEPARATOR + STATE_ACTIVE + IMAGE_EXTENSION;
    public static final String TURTLE_LEFT_NORMAL = Resources.BASE_PATH + BASE_PATH + CHARACTER_TURTLE + SEPARATOR + DIRECTION_LEFT + SEPARATOR + STATE_NORMAL + IMAGE_EXTENSION;
    public static final String TURTLE_RIGHT_ACTIVE = Resources.BASE_PATH + BASE_PATH + CHARACTER_TURTLE + SEPARATOR + DIRECTION_RIGHT + SEPARATOR + STATE_ACTIVE + IMAGE_EXTENSION;
    public static final String TURTLE_RIGHT_NORMAL = Resources.BASE_PATH + BASE_PATH + CHARACTER_TURTLE + SEPARATOR + DIRECTION_RIGHT + SEPARATOR + STATE_NORMAL + IMAGE_EXTENSION;

    public enum Character {
        MARIO,
        MUSHROOM,
        TURTLE
    }

    public enum Direction {
        RIGHT,
        LEFT
    }

    public enum State {
        ACTIVE,
        JUMP,
        NORMAL,
        DEAD
    }

    public static String getImagePath(Character character, Direction direction, State state) {
        return Resources.BASE_PATH +
                BASE_PATH +
                getCharacter(character) +
                SEPARATOR +
                getDirection(direction) +
                SEPARATOR +
                getState(state) +
                IMAGE_EXTENSION;
    }

    private static String getCharacter(Character character) {
        switch (character) {
            case MARIO:
                return CHARACTER_MARIO;
            case MUSHROOM:
                return CHARACTER_MUSHROOM;
            case TURTLE:
                return CHARACTER_TURTLE;
            default:
                return "";
        }
    }

    private static String getDirection(Direction direction) {
        switch (direction) {
            case RIGHT:
                return DIRECTION_RIGHT;
            case LEFT:
                return DIRECTION_LEFT;
            default:
                return "";
        }
    }

    private static String getState(State state) {
        switch (state) {
            case ACTIVE:
                return STATE_ACTIVE;
            case DEAD:
                return STATE_DEAD;
            case JUMP:
                return STATE_JUMP;
            case NORMAL:
                return STATE_NORMAL;
            default:
                return "";
        }
    }

}
