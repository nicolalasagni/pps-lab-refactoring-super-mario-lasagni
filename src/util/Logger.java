package util;

import java.io.PrintStream;

/**
 * Created by nicolalasagni on 03/03/2017.
 */
public class Logger {

    private static final String LEFT_PAR = "[";
    private static final String RIGHT_PAR = "]";
    private static final String SEPARATOR = ":";
    private static final String SPACE = " ";

    private static PrintStream console() {
        return System.out;
    }

    private static String separator() {
        return SEPARATOR;
    }

    private static String space() {
        return SPACE;
    }

    private static String getTagFormatted(String tag) {
        return LEFT_PAR + tag + RIGHT_PAR;
    }

    private static void print(String s) {
        console().print(s);
    }

    private static void printAndTerminateLine(String s) {
        console().println(s);
    }

    private static void printTagFormatted(String tag) {
        print(getTagFormatted(tag));
        print(separator());
        print(space());
    }

    public static void print(String tag, String message, Throwable throwable) {
        printTagFormatted(tag);
        print(message);
        print(space());
        printAndTerminateLine(throwable.getMessage());
    }

    public static void print(String tag, String message) {
        printTagFormatted(tag);
        printAndTerminateLine(message);
    }

    public static void print(String tag, Throwable throwable) {
        printTagFormatted(tag);
        printAndTerminateLine(throwable.getMessage());
    }

}
