package app;

import game.builder.GameBuilder;
import game.model.ViewConfiguration;

public class Launcher {

    private static final String TITLE = "Super Mario";
    private static final int VIEW_WIDTH = 700;
    private static final int VIEW_HEIGHT = 360;

    public static void main(String[] args) {
        ViewConfiguration viewConfiguration = new ViewConfiguration(TITLE, VIEW_WIDTH, VIEW_HEIGHT);
        GameBuilder gameBuilder = new GameBuilder(viewConfiguration);
        gameBuilder.startGame();
    }

}
